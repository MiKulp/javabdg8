package com.sda.sololearn.piglatin;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class PigLatinMethodsTest {

PigLatinMethods pigLatinMethods = new PigLatinMethods();
String stringToTest = "biblioteka";
String stringToTestArrays = "biblioteka ukw";

    @Test
    @DisplayName("Test method removeFirstLetter")
    public void replaceFirstLetterInPigLatinTest () {
        Assert.assertEquals("ibliotekab",pigLatinMethods.removeFirstLetterAndAddInTheEndOfString(stringToTest));
    }

    @Test
    @DisplayName("Test method addLettersToString")
    public void addLettersToStringTest () {
        Assert.assertEquals("bibliotekaay", pigLatinMethods.addLettersToString(stringToTest));
    }

    @Test
    @DisplayName("Create array Test.")
    public void makeArrayFromStringTest () {
        String [] array = new String[2];
        array[0] = "biblioteka";
        array[1] = "ukw";
        Assertions.assertArrayEquals(array, pigLatinMethods.makeArrayFromString(stringToTestArrays));
    }

}