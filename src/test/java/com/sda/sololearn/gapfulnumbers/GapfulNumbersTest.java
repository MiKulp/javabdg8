package com.sda.sololearn.gapfulnumbers;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class GapfulNumbersTest {


    GapfulNumbers gapfulNumbers = new GapfulNumbers();

    @DisplayName("Test when number is gapeful. Expect = true ")
    @ParameterizedTest
    @ValueSource(strings = {"192", "583"})
    public void isGapfulNumberTest(String string) {
        Assertions.assertThat(gapfulNumbers.isGapfulNumbers(string)).isTrue();
    }

    @DisplayName("Test when number is gapeful. Expect = true ")
    @ParameterizedTest
    @ValueSource(strings = {"210", "173"})
    public void isNotGapfulNumberTest(String string) {
        Assertions.assertThat(gapfulNumbers.isGapfulNumbers(string)).isFalse();
    }

}
