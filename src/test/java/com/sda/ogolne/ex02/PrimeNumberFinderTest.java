package com.sda.ogolne.ex02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class PrimeNumberFinderTest {

    @Test
    public void primeNumberTest () {
        int n = 1531;
        PrimeNumberFinder pnf = new PrimeNumberFinder();
        Assertions.assertEquals(pnf.primeNumberFinder(n),1523);
    }


}