package com.sda.ogolne.ex03;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AddressBookEngineTest {

    List<Contact> listOfContacts = Arrays.asList(
            new Contact("Mi", "No", "email@o2.pl", "123 456 7892"),
            new Contact("Ka", "Kp", "email2@o2.pl", "123 456 7891"),
            new Contact("Wa", "Cz", "email3@o2.pl", "123 456 7893")

    );

    @Test
    void searchContactInList() {
        AddressBookEngine  abe = new AddressBookEngine();
        abe.searchContactInList(listOfContacts,"123 456 7891");
    }
}