package com.sda.recursion.ex01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class RecursionEngineTest {
    String testString;
    RecursionEngine re = new RecursionEngine();

    RecursionEngineTest() {
        testString = re.randomStringMaker(10000);
    }

    @Test
    public void stringRecursionTest() {
        Assertions.assertEquals("QWERTY", re.reverseOrderWithRecursion("YTREWQ"));
    }

    @Test
    public  void stringWithoutRecursionEngineTest () {
        Assertions.assertEquals("QWERTY", re.reverseOrderWithoutRecursion("YTREWQ"));
    }

    @Test
    public void stringWithoutRecursionTestThrowStackOverflowEx() {
        Assertions.assertDoesNotThrow((ThrowingSupplier<StackOverflowError>) StackOverflowError::new,()->re.reverseOrderWithoutRecursion(testString));
    }

    @Test
    public void stringRecursionTestThrowStackOverflowEx() {
        Assertions.assertThrows(StackOverflowError.class, () -> re.reverseOrderWithRecursion(testString));
    }

    @Test
    public  void stringWithoutRecursionEngineParamTest () {
        Assertions.assertEquals("QWERTY", re.reverseOrderWithoutRecursion("YTREWQ"));
    }


}