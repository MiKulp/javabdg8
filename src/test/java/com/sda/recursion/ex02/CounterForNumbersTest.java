package com.sda.recursion.ex02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CounterForNumbersTest {
    int test = 100;
    int score = 5050;
    int maxValue = 100000;
    CounterForNumbers counter = new CounterForNumbers();

    @Test
    public void countElementsWithoutRecursion() {
        Assertions.assertEquals(score, counter.countNumbersWithoutRecursion(test));
    }

    @Test
    public void countElementsWithRecursion() {
        Assertions.assertEquals(score, counter.countNumbersWithRecursion(test));
    }

    @Test
    public void stackOverFlowExpectedInRecursionMethod() {
        Assertions.assertThrows(StackOverflowError.class, () -> counter.countNumbersWithRecursion(maxValue));
    }

    @Test
    public void stackOverFlowNotExpectedInNonRecursionMethod() {
        Assertions.assertDoesNotThrow(() -> counter.countNumbersWithoutRecursion(maxValue));
    }


}