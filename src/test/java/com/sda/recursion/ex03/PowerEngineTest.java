package com.sda.recursion.ex03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PowerEngineTest {
    long testInteger = 4;
    PowerEngine pe = new PowerEngine();

    @Test
    public void strongWithoutRecursion() {
        Assertions.assertEquals(24, pe.powerWithoutRecursion(testInteger));
    }

    @Test
    public void strongWithRecursion() {
        Assertions.assertEquals(24, pe.powerWithRecursion(testInteger));
    }

}