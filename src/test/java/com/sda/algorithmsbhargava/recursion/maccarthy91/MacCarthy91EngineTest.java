package com.sda.algorithmsbhargava.recursion.maccarthy91;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

class MacCarthy91EngineTest {

    @RepeatedTest(2000)
    public void macCarthyTest() {

        long n = -123456L;
        Assertions.assertEquals(91L,MacCarthy91Engine.macCarthyRecursionAlgorithm(n));
        Assertions.assertNotEquals(92L, MacCarthy91Engine.macCarthyRecursionAlgorithm(n));
    }

}