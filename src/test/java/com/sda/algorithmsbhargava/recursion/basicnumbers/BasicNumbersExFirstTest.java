package com.sda.algorithmsbhargava.recursion.basicnumbers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BasicNumbersExFirstTest {

    @Test

    public void numberFirstRec() {
        BasicNumbersExFirst first = new BasicNumbersExFirst();
        Assertions.assertEquals(15, first.sumNumbersWithRec(5));
        Assertions.assertNotEquals(1, first.sumNumbersWithRec(2));
    }

}