package com.sda.algorithms.ex05;

import lombok.ToString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class DistanceInCoordinateSystemTest {

    @Test
    public void distanceCoordinateSystemTest() {
        DistanceInCoordinateSystem system = new DistanceInCoordinateSystem();
        PointInCoordinateSystem firstPoint = new PointInCoordinateSystem(2, 5);
        PointInCoordinateSystem secondPoint = new PointInCoordinateSystem(5, 9);
        Assertions.assertEquals(5, system.distanceBetweenPointsInCoordinateSystem(firstPoint, secondPoint));
    }

    @Test
    public void distanceInRandomListFactoryTest() {
        List<PointInCoordinateSystem> testList = Arrays.asList(
                new PointInCoordinateSystem(2, 3),
                new PointInCoordinateSystem(3, 3),
                new PointInCoordinateSystem(666, 999),
                new PointInCoordinateSystem(777, 888),
                new PointInCoordinateSystem(555, 777));

        DistanceInCoordinateSystem distance = new DistanceInCoordinateSystem();
        Assertions.assertEquals(1.0, distance.distanceInRandomListFactory(testList).get(0).distance);
    }
}