package com.sda.algorithms.ex01;

import org.junit.jupiter.api.*;

class QuadraticEquationScoreFinderTest {

    private static QuadraticEquationScoreFinder scoreFinder;

    @BeforeAll
    static void before() {
        scoreFinder = new QuadraticEquationScoreFinder();
    }

    @Test
    public void deltaFindChecker() {
        Assertions.assertEquals(-8, scoreFinder.deltaFinder(1, 2, 3));


    }


    @Test
    public void firstArgumentTest() {
        QuadraticEquationScoreFinder scoreFinder = new QuadraticEquationScoreFinder();
        Assertions.assertEquals(-3, scoreFinder.firstArgumentCounter(1, 2, 16));
    }

    @Test
    public void firstArgumentTestFalse() {
        QuadraticEquationScoreFinder scoreFinder = new QuadraticEquationScoreFinder();
        Assertions.assertNotEquals(3, scoreFinder.firstArgumentCounter(1, 2, 16));
    }

    @Tag("some tag")
    @Test
    public void secondArgumentTest() {
        QuadraticEquationScoreFinder scoreFinder = new QuadraticEquationScoreFinder();
        Assertions.assertEquals(1, scoreFinder.secondArgumentCounter(1, 2, 16));
    }

    @Test
    public void secondArgumentTestFalse() {
        QuadraticEquationScoreFinder scoreFinder = new QuadraticEquationScoreFinder();
        Assertions.assertNotEquals(11, scoreFinder.secondArgumentCounter(1, 2, 16));
    }

    @Test
    public void onlyOneArgumentTest() {
        QuadraticEquationScoreFinder scoreFinder = new QuadraticEquationScoreFinder();
        Assertions.assertEquals(-0.5, scoreFinder.onlyOneArgumentCounter(4, 4));
    }

    @Test
    @TestFactory
    public void onlyOneArgumentTestFalse() {
        QuadraticEquationScoreFinder scoreFinder = new QuadraticEquationScoreFinder();
        Assertions.assertNotEquals(-0.5, scoreFinder.onlyOneArgumentCounter(4, 5));
    }
}
