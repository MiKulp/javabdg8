package com.sda.general.ex05;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class PalindromeCheckerTest {
    PalindromeChecker pc = new PalindromeChecker();

    @Test
    void shouldReverseInputString() {
        String exampleString = "abcd";
        String scoreString = "dcba";
        String anyOtherScore = "QWERTY";
        assertEquals(scoreString, pc.reverseInputString(exampleString));
        assertNotEquals(anyOtherScore, pc.reverseInputString(exampleString));
    }

    @Test
    void palindromePreparator() {
        String typicalString = "Ala ma kota";
        String expectedScore = "alamakota";
        String falseScoreString = "Alamakota";
        assertEquals(expectedScore, pc.palindromePreparator(typicalString));
        assertNotEquals(falseScoreString, pc.palindromePreparator(typicalString));
    }

    @ParameterizedTest
    @ValueSource (strings = {
            "A to kawa kota",
            "A to kiwi zdziwi kota",
            "Może jeż łka jak łże jeżom",
            "Zakopane chce na pokaz"})
    void isPalindrome(String string) {
        Assertions.assertTrue(pc.isPalindrome(string));
    }
}