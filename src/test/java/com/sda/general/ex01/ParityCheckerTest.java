package com.sda.general.ex01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;



class ParityCheckerTest {

    @Test
    public void shouldReturnFalseOnIsOddWhenEvenNumberIsProvided() {
        boolean result = new ParityChecker().isOdd(2);
        Assertions.assertFalse(result);

    }

    @Test
    public void shouldReturnTrueOnIsOddWhenEvenNumberIsProvided() {
        boolean result = new ParityChecker().isOdd(11);
        Assertions.assertTrue(result);

    }

    @Test
    public void shouldReturnFalseOnIsEvenWhenEvenNumberIsProvided() {
        boolean result = new ParityChecker().isEven(11);
        Assertions.assertFalse(result);
    }

    @Test
    public void shouldReturnTrueOnIsEvenWhenEvenNumberIsProvided() {
        boolean result = new ParityChecker().isEven(20);
        Assertions.assertTrue(result);

    }

}