package com.sda.general.ex03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseWordTest {

    @Test
    public void isReverseValid () {

        String test = "abc";
        String oczkiwanyWynik = "cba";
        String rw = new ReverseWord().reversingWord(test);
        Assertions.assertEquals(rw,oczkiwanyWynik);

    }

    @Test
    public void isReverseInvalid () {

        String test = "abc";
        String oczkiwanyWynik = "abc";
        String rw = new ReverseWord().reversingWord(test);
        Assertions.assertNotEquals(rw, oczkiwanyWynik);


    }

}