package com.sda.general.ex02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PrimeNumbersCheckerTest {


    @Test
    public void isPrimeNumberValidMethodWithPrimeNumber () {
        boolean result = new PrimeNumbersChecker().isPrimeNumber(23);
        Assertions.assertTrue(result);
    }

    @Test
    public void isPrimeNumberValidMethodWithNotPrimeNumber () {
        boolean result = new PrimeNumbersChecker().isPrimeNumber(30);
        Assertions.assertFalse(result);
    }

}