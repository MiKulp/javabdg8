package com.sda.optional.ex01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class OptionalNullTesterTest {

    @Test
    @DisplayName("Test pierwszego zadania Optionals. Oczekiwany wynik true.")
    public void isNullMethodTester() {
        OptionalNullTester nullTester = new OptionalNullTester();
        Object object = null;
        Assertions.assertTrue(nullTester.isNull(object));
    }

    @Test
    @DisplayName("Drugi test pierwszego zadania. Oczekiwany wynik - true.")
    public void isNotNullMethodTsster () {
        Integer integerNotNull = 18;
        OptionalNullTester nullTester = new OptionalNullTester();
        Assertions.assertFalse(nullTester.isNull(integerNotNull));
    }

}