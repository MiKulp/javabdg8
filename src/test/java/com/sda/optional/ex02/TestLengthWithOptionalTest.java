package com.sda.optional.ex02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestLengthWithOptionalTest {

    @Test
    @DisplayName("Test metody mierzacej długosc tekstu wg wskazowek trenera. ")
    public void testOfTestLengthWithOptionals () {
        String text = null;
        TestLengthWithOptional tlwo = new TestLengthWithOptional();
        tlwo.stringLnthWithAnotherMethod(text);
        Assertions.assertEquals(-1,tlwo.stringLnthWithAnotherMethod(text));
    }

    @Test
    @DisplayName("Test metody mierzacej długosc tekstu wg wskazowek trenera. ")
    public void testOfTestLengthWithOptionalsArgNotNull () {
        String text = "Some text";
        TestLengthWithOptional tlwo = new TestLengthWithOptional();
        tlwo.stringLnthWithAnotherMethod(text);
        Assertions.assertEquals(9,tlwo.stringLnthWithAnotherMethod(text));
    }

}