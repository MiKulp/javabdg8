package com.sda.optional.ex03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OptionalAndColorChangerTest {

    @DisplayName("Testy dla obiektu null.")
    @Test
    public void changeWithNull () {
        String testowy = null;
        Colors wynik = Colors.UNKNOWN;
        OptionalAndColorChanger changer = new OptionalAndColorChanger();
        Assertions.assertEquals(wynik, changer.colorConverter(testowy));
    }

    @DisplayName("Testy dla obiektu NotNull.")
    @Test
    public  void changeWithNotNull () {
        String testowy = "red";
        Colors wynik = Colors.RED;
        OptionalAndColorChanger changer = new OptionalAndColorChanger();
        Assertions.assertEquals(wynik, changer.colorConverter(testowy));
    }

    @DisplayName("Testy dla obiektu NotNull.")
    @Test
    public  void changeWithNotNullButWrongData () {
        String testowy = "pink";
        OptionalAndColorChanger changer = new OptionalAndColorChanger();
       // Assertions.assertThrows(changer.colorConverter(testowy));
        Assertions.assertThrows(IllegalArgumentException.class, ()->changer.colorConverter(testowy));
        //testowaną metodę można uruchomić tylko za pomocą lambdy

    }






}