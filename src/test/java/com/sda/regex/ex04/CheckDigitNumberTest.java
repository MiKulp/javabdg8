package com.sda.regex.ex04;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckDigitNumberTest {

    @Test
    public void haveOnlyThreeDigits () {
        String example = "123";
        CheckDigitNumber digitNumber = new CheckDigitNumber();
        Assertions.assertTrue(digitNumber.haveOnlyThreeDigits(example));
    }

    @Test
    public void haveMoreThanThreeDigits () {
        String example = "123456789";
        CheckDigitNumber digitNumber = new CheckDigitNumber();
        Assertions.assertFalse(digitNumber.haveOnlyThreeDigits(example));
    }

}