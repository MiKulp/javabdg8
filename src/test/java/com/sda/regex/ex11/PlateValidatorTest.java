package com.sda.regex.ex11;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class PlateValidatorTest {

    @DisplayName("Test na poprawnych tablicach.")
    @ParameterizedTest
    @ValueSource(strings = {"CB3456J", "CB34212", "WY640WI"})
    public void isCorrectPlate (String string) {
        PlateValidator pl = new PlateValidator();
        Assertions.assertTrue(pl.validate(string));
    }

    @DisplayName("Test na nieprawidlowych wartościach wpisanych w tablice.")
    @ParameterizedTest
    @ValueSource (strings = {"CBS3456", "W1234YU", "CC14WYG"})
    public void IsIncorrectPlate (String string) {
        PlateValidator pl = new PlateValidator();
        Assertions.assertFalse(pl.validate(string));
    }

}