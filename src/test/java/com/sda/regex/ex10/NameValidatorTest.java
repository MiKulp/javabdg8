package com.sda.regex.ex10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import static org.junit.jupiter.api.Assertions.*;

class NameValidatorTest {

    @ParameterizedTest
    @ValueSource (strings = {"monika87", "janeK"})
    public void containsInvalidSigns (String string) {
        NameValidator nv = new NameValidator();
        Assertions.assertFalse(
        nv.validate(string));
    }

    @ParameterizedTest
    @ValueSource (strings = {"Michał", "Jan", "Karol"})
    public void startWithLower (String string) {
        NameValidator nv = new NameValidator();
        Assertions.assertTrue(nv.validate(string));
    }

}