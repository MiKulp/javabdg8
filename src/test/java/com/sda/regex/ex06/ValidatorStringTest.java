package com.sda.regex.ex06;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorStringTest {

    @Test
    public void haveOtherSigns () {
        ValidatorString vd = new ValidatorString();
        Assertions.assertFalse(vd.validator("!@#$%"));
    }

    @Test
    public void haveDigitsAndCases () {
        ValidatorString vd = new ValidatorString();
        Assertions.assertTrue(vd.validator("123ABCabc"));
    }



}