package com.sda.regex.ex01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorLowerUpperCaseTest {

    @Test
    public void onlyUpperCase () {
        String example = "ABC";
        ValidatorLowerUpperCase validatorLowerUpperCase = new ValidatorLowerUpperCase();
        Assertions.assertTrue(validatorLowerUpperCase.isOnlyLower(example));
    }

    @Test
    public void onlyLowerCase () {
        String example = "abc";
        ValidatorLowerUpperCase validatorLowerUpperCase = new ValidatorLowerUpperCase();
        Assertions.assertFalse(validatorLowerUpperCase.isOnlyLower(example));
    }

    @Test
    public void mixedCase () {
        String example = "aBc";
        ValidatorLowerUpperCase validatorLowerUpperCase = new ValidatorLowerUpperCase();
        Assertions.assertFalse(validatorLowerUpperCase.isOnlyLower(example));
    }




}