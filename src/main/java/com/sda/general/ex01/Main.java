package com.sda.general.ex01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Wprowadź liczbę do sprawdzenia: ");
        Scanner scanner = new Scanner(System.in);
        Integer number = scanner.nextInt();
        ParityChecker parityChecker = new ParityChecker();
        System.out.println("Liczba parzysta: " + parityChecker.isEven(number));
        System.out.println("Liczba nieparzysta: " + parityChecker.isOdd(number));

    }
}
