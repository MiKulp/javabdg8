package com.sda.general.ex01;

import lombok.extern.slf4j.Slf4j;

@Slf4j

public class ParityChecker {

    public boolean isOdd(Integer number) {
        log.info("Checking isOdd()");
        log.debug("Checking number " + number + " if is odd. Result"  +(number%2 != 0));
        return number % 2 != 0;

    }

    public boolean isEven(Integer number) {
        log.info("Checking isEven()");
        log.debug("Checking number " +number+ " if is even. Result " +(number%2 == 0));
        return number % 2 == 0;

    }
}
