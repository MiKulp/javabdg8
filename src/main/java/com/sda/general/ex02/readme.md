Napisz program, który pobiera liczbę od użytkownika i sprawdza czy wprowadzona
liczba jest liczbą pierwszą. Program wyświetla komunikat
“Podana liczba <liczba> jest liczbą pierwszą”
lub
“Podana liczba <liczba> nie jest liczbą pierwszą”