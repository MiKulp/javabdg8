package com.sda.general.ex02;

public class PrimeNumbersChecker {
    Integer n;

    public boolean isPrimeNumber(Integer n) {

        boolean wynik = false;
        for (int i = 2 ; i < n - 1 ; i++) {

            if (n % i == 0) {
                wynik = false;
                break;
            } else {
                wynik = true;
            }

        }

        if (wynik)
            System.out.printf("Podana liczba %d jest liczbą pierwszą", n);
        else {
            System.out.printf("Podana liczba %d nie jest liczbą pierwszą", n);
        }
        return wynik;



    }

}
