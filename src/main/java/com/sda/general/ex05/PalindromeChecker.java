package com.sda.general.ex05;

import java.sql.SQLOutput;

public class PalindromeChecker {

    public String reverseInputString(String givenText) {
        String text = givenText.replaceAll("(\\s)", "");
        int iter = 0;
        StringBuilder score = new StringBuilder("");

        for (int i = text.length() - 1 ; i >= 0 ; i--) {
            score.append(text.charAt(i));
            iter++;
        }
        return score.toString().toLowerCase();
    }

    public String palindromePreparator(String string) {
        return string.replaceAll("\\s", "").toLowerCase();
    }

    public boolean isPalindrome(String text) {

        String typicallText = palindromePreparator(text);
        System.out.println(typicallText);
        String reversedText = reverseInputString(text);
        System.out.println(reversedText);

        if (typicallText.equals(reversedText)) {
            System.out.printf("%s, ten zwrot jest Palindromem. ", text);
            return true;
        } else {
            System.out.printf("%s, ten zwrot nie jest Palindromem. ", text);
            return false;
        }

    }

}
