package com.sda.general.ex05;

public class Main {

    public static void main(String[] args) {
        PalindromeChecker palindromeChecker = new PalindromeChecker();
        String example = "Si nummi immunis";
        palindromeChecker.isPalindrome(example);
    }
}
