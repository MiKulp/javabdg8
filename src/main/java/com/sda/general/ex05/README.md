Napisz program, który pobiera od użytkownika wyraz lub zdanie i sprawdza czy
wprowadzony ciąg znaków jest palindromem. Przykładowy palindrom to “kajak” lub
“Może jutro ta dama da tortu jeżom”. Program po analizie wyświetla komunikat:
Wprowadzony ciąg znaków “<wprowadzone_znaki>” jest palindromem
lub
Wprowadzony ciąg znaków “<wprowadzone_znaki>” nie jest palindromem