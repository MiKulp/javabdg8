package com.sda.ogolne.ex02;

public class PrimeNumberFinder {

    public boolean isPrimeNumber(Integer n) {
        boolean wynik = false;

        for (int i = 2 ; i < n - 1 ; i++) {
            if (n % i == 0) {
                wynik = false;
                break;
            } else {
                wynik = true;
            }
        }

        return wynik;
    }

    public int highPrimeNumber(int n) {
        int number = ++n;
        boolean test = false;
        boolean wynik;

        while (!test) {
            wynik = isPrimeNumber(number);
            if (wynik) {
                //test = true;
                break;
            } else {
                number++;
                test = false;
            }
        }
        return number;
    }

    public int lowPrimeNumber(int n) {
        int number = --n;
        boolean test = false;
        boolean wynik;

        while (!test) {
            wynik = isPrimeNumber(number);
            if (wynik) {
                //test = true;
                break;
            } else {
                number--;
                test = false;
            }
        }
        return number;
    }

    public int primeNumberFinder (int n) {
        int lowPrime = n - lowPrimeNumber(n);
        int highPrime = highPrimeNumber(n) - n;

        if (lowPrime > highPrime) {
            return highPrimeNumber(n);
        }
        else if (lowPrime < highPrime ) {
            return lowPrimeNumber(n);
        }
        else
            return highPrimeNumber(n);

    }

}
