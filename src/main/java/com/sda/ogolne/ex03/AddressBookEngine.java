package com.sda.ogolne.ex03;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddressBookEngine {

    Utils utils = new Utils();
    Contact contact = new Contact("Michal", "Kulpa", "typicalMail@wp.pl", "666666666");
    List<Contact> listOfContacts = Arrays.asList(
            new Contact("Mi", "No", "email@o2.pl", "123456789"),
            new Contact("Ka", "Kp", "email2@o2.pl", "123456789"),
            new Contact("Wa", "Cz", "email3@o2.pl", "123456789")

    );

    /**
     * Ta metoda jest gotowa.Pobiera element z pliku JSON i przekształac go na listę adresową.
     *
     * @param path
     * @return
     * @throws IOException
     */

    public List<Contact> makeListOfContactsFromJSONfile(String path) throws IOException {
        List<Contact> contactList;
        ObjectMapper mapper = new ObjectMapper();
        contactList = mapper.readValue(new File(path), List.class);
        /*for (int i = 0 ; i < contactList.size() ; i++) {
            System.out.println(contactList.get(i));
        }

         */
        return contactList;
    }

    public void createJSONwithObjects() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File("C:\\SDA_testy_IO\\testy.json"), listOfContacts);
    }

    public void takeObjectFromListByFirstName(List<Contact> list, String firstName) {
        list.stream().filter(contact1 -> {
            return contact1.first_name.equals(firstName);
        }).forEach(System.out::println);
    }

    public void takeObjectFromListByEmail(List<Contact> list, String email) {
        list.stream().filter(contact1 -> {
            return contact1.email.equals(email);
        }).forEach(System.out::println);
    }

    public void takeObjectFromListByPhoneNumber(List<Contact> list, String phoneNumber) {
        list.stream().filter(contact1 -> {
            return contact1.phone_number.equals(phoneNumber);
        }).forEach(System.out::println);
    }


    public List<Contact> converter(List<Contact> firstList) {
        ObjectMapper om = new ObjectMapper();
        List<Contact> scoreList = om.convertValue(firstList, new TypeReference<List<Contact>>() {
        });
        return scoreList;
    }

    public void searchContactInList(List<Contact> list, String string) {

        Pattern patternName = Pattern.compile("[[A-ZĄĆĘŁŃÓŚŻŻ]{1}][a-ząćęłńóśźż]+");
        Pattern patternEmail = Pattern.compile("[A-Za-z0-9.% +-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Pattern patternPhoneNumber = Pattern.compile("[\\d]{3}+\\s[\\d]{3}+\\s[\\d]{4}");
        Matcher matcherName = patternName.matcher(string);
        Matcher matcherEmail = patternEmail.matcher(string);
        Matcher matcherPhoneNumber = patternPhoneNumber.matcher(string);

        if (matcherName.matches()) {
            System.out.println("Takst zawiera imie albo nazwisko. ");
            takeObjectFromListByFirstName(list, string);
        } else if (matcherEmail.matches()) {
            System.out.println("Tekst zawiera adres email. ");
            takeObjectFromListByEmail(list, string);
        } else if (matcherPhoneNumber.matches()) {
            System.out.println("String to numer telefonu. ");
            takeObjectFromListByPhoneNumber(list, string);
        } else {
            System.out.println("Brak rekordu. ");

        }

    }

    public String makeJSONfileFromList(List<Contact> mainList) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

        String json = mapper.writeValueAsString(mainList);
        return json;
    }

    public void deleteJSONfile() {
        File file = new File(utils.path);

        if (file.exists()) {
            file.delete();
        } else {
            System.out.println("Nie ma pliku");
        }
    }

    public void writeJSONfile(List<Contact> listOfContacts) throws IOException, JSONException {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        Path path = Paths.get(utils.path);
        File file = new File(String.valueOf(path));

        try {
            for (Contact c : listOfContacts
            ) {
                JSONObject contactJSON = new JSONObject();
                contactJSON.put("first_name", c.getFirst_name());
                contactJSON.put("last_name", c.getLast_name());
                contactJSON.put("email", c.getEmail());
                contactJSON.put("phone_number", c.getPhone_number());
                // System.out.println(contactJSON);
                jsonArray.put(contactJSON);
            }

            jsonObject.put("ContactList", jsonArray);

        } catch (JSONException jse) {
            jse.printStackTrace();
        }

        //deleteJSONfile();
        // file.createNewFile();

        System.out.println(jsonArray);

    }

}
