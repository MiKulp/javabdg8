Napisz program, który będzie przechowywał kontakty (imię, nazwisko, numer
telefonu, adres e-mail).

a. Kontakty mogą być predefiniowane z poziomu kodu i zapisywane do pliku.

b. Kontakty mogą być odczytywane z dysku.
Użyj formatu JSON korzystając z biblioteki jackson-mapper-asl Skorzystaj z
https://www.mkyong.com/java/how-to-convert-java-object-to-from-json-jackso
n/

c. Program umożliwia wyszukiwanie kontaktu. pyta o tryb wyszukiwania (po
nazwie, numerze telefonu lub adresie e-mail).

d. Program prosi o wpisanie dowolnej wartości i wypisuje wszystkie powiązane z
nim informacje w postaci: Imię: imię Nazwisko:nazwisko, tel.: numer telefonu,
e-mail: adres e-mail. Wyszukiwanie odbywa się poprzez dopasowywanie
tekstu do wszystkich elementów kontaktu.

e. *Dodać możliwość wprowadzania kontaktu i zapisu do pliku