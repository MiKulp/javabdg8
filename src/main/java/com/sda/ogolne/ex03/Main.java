package com.sda.ogolne.ex03;

import org.codehaus.jettison.json.JSONException;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, JSONException {
        String path = "C:\\SDA_testy_IO\\MOCK_DATA.json";
        AddressBookEngine abe = new AddressBookEngine();

        List<Contact> lista = abe.makeListOfContactsFromJSONfile(path);

        /*Contact newContact = new Contact();
        newContact.setFirst_name("Michal");
        newContact.setLast_name("Kowalski");
        newContact.setEmail("kowalski@gmail.com");
        newContact.setPhone_number("998 262 1234");
        lista.add(newContact);
        System.out.println(lista.size());

         */
        abe.makeJSONfileFromList(lista);

        abe.createJSONwithObjects();

        List<Contact> lastList = abe.converter(lista);

        System.out.println(lista);

        System.out.println(lastList);

        String json = abe.makeJSONfileFromList(lastList);

        abe.writeJSONfile(lastList);

    }
}
