package com.sda.ogolne.ex03;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString

public class Contact {
    String first_name;
    String last_name;
    String email;
    String phone_number;

}


