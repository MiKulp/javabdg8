package com.sda.ogolne.ex03;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString

public class ContactList {
    public List<Contact> contactList = new ArrayList<>();

}
