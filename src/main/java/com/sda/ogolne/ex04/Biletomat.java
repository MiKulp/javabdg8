package com.sda.ogolne.ex04;

import java.util.*;

public class Biletomat {

    public void dzielenieReszty(int reszta) {
        Integer[] listaNominalow = {200, 100, 50, 20, 10, 5, 2};
        int i = 0;
        int j = 0;
        int wydanaReszta = 0;
        SortedMap<Integer, Integer> mapaWynikow = new TreeMap<>();

        while (reszta > 0) {
            if (reszta - listaNominalow[i] >= 0 && reszta >= 2) {
                wydanaReszta += listaNominalow[i];
                reszta -= listaNominalow[i];
                j++;
                mapaWynikow.put(listaNominalow[i], j);

            } else if (reszta - listaNominalow[i] < 0 && reszta >= 2) {
                i++;
                j = 0;

            } else if (reszta < 2) {
                wydanaReszta += reszta;
                mapaWynikow.put(1, 1);
                reszta = 0;
            }
        }

        System.out.println("Poniżej jest lista wypłaconych środków - twoja reszta. ");
        System.out.println("W pierwszej kolumnie nominał, a w drugiej wydana ilość. ");
        for (Map.Entry<Integer, Integer> entry : mapaWynikow.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            System.out.printf("%-3d x%2d %n", key, value);
        }

    }

    public void przyjmowanieZamowienia() {
        int numberOfTickets = 0;
        int cashAmount = 0;
        int reszta = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Ile biletow chcesz kupic?");
        numberOfTickets = sc.nextInt();
        System.out.println("Proszę, wprowadz wplacona kwote.");
        cashAmount = sc.nextInt();
        reszta = cashAmount - (numberOfTickets * 4);
        System.out.printf("Kupiłeś %d bilety za 4 PLN. Otrzymasz %2d reszty. %n", numberOfTickets, reszta);
        dzielenieReszty(reszta);
    }
}
