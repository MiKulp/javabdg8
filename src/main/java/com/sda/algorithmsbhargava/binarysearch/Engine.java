package com.sda.algorithmsbhargava.binarysearch;

public class Engine {
    public static int i = 1;
    public int binarySearchInSimplyArray(int [] array, int item) {

        int low = 0, high = array.length - 1;

        while (low <= high) {

            int mid = (low + high ) / 2;

            if (array[mid] == item)
                return mid;
            if (array[mid] < item)
                low = mid + 1;
            else
                high = mid - 1;

            System.out.println("Wyszukuję.." +i);
            i++;
        }
        return -1;
    }


    public int[] createArray(int n) {
        int[] array = new int[n];

        for (int i = 0 ; i < n ; i++) {
            array[i] = i * i;
        }
        return array;
    }

}
