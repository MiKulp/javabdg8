package com.sda.algorithmsbhargava.binarysearch;

public class Main {
    public static void main(String[] args) {
        Engine engine = new Engine();
        Integer[] array = new Integer[]{1, 3, 5, 7, 9, 11, 23, 34, 44, 59};
        int[] secondaryArray;
        secondaryArray = engine.createArray(128);
        System.out.println(secondaryArray.length);

        for (int n: secondaryArray
             ) {
            System.out.printf("%d, ", n);
        }

        System.out.printf("%n");

       int wynik = engine.binarySearchInSimplyArray(secondaryArray, 841);
        System.out.println(wynik);
    }
}
