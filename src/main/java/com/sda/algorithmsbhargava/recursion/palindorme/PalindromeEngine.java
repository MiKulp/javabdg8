package com.sda.algorithmsbhargava.recursion.palindorme;

public class PalindromeEngine {

    public boolean palindromeChecker (String string, int s, int e) {

        if (s == e)
            return true;

        if (string.charAt(s) != string.charAt(e))
            return false;

        if (s < e + 1)
            return palindromeChecker(string, s + 1, e - 1);

        return true;

    }
}
