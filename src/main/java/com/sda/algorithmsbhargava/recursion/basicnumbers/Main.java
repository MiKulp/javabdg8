package com.sda.algorithmsbhargava.recursion.basicnumbers;

public class Main {
    public static void main(String[] args) {

        BasicNumbersExFirst first = new BasicNumbersExFirst();
        BasicNumbersExSecond second = new BasicNumbersExSecond();

        System.out.println(first.sumNumbersWithRec(5));

        System.out.println(second.isPrimeNumberWithRec(48, 2));
    }
}
