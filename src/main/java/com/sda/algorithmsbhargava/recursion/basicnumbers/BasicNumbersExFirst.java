package com.sda.algorithmsbhargava.recursion.basicnumbers;

public class BasicNumbersExFirst {

    public int sumNumbersWithRec (int n) {

        if (n < 2)
            return n;

        else
        return n + sumNumbersWithRec(n-1);
    }
}
