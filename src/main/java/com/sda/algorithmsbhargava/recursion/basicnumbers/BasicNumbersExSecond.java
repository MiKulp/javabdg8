package com.sda.algorithmsbhargava.recursion.basicnumbers;

public class BasicNumbersExSecond {

    public boolean isPrimeNumberWithRec(int n, int i)  {

        if (n <= 2)
            return (n == 2) ? true : false;
        if (n % i == 0)
            return false;
        if (i * i > n)
            return true;

        return isPrimeNumberWithRec(n, i + 1);
    }
}
