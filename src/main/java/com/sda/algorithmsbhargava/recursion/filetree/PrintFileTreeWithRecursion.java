package com.sda.algorithmsbhargava.recursion.filetree;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class PrintFileTreeWithRecursion {


    public void printFileTreeNewVersion (File [] files) {
        for (File file : files) {
            if (file.isDirectory()) {
                printFileTreeNewVersion(file.listFiles());
            } else {
                System.out.println("File: " + file.toString());
            }
        }
    }


}