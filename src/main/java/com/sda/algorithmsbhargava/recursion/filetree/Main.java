package com.sda.algorithmsbhargava.recursion.filetree;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String string = "C:\\Users\\mkulp\\Desktop\\kursTutoriale";
        String stringDrugi = "C:";
        PrintFileTreeWithRecursion print = new PrintFileTreeWithRecursion();
        File[] file = new File(string).listFiles();
        File[] fileDrugi = new File(stringDrugi).listFiles();

        if (file!=null) {
            print.printFileTreeNewVersion(file);
        }

    }
}
