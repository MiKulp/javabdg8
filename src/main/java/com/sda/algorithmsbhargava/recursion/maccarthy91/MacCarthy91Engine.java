package com.sda.algorithmsbhargava.recursion.maccarthy91;

public class MacCarthy91Engine {

    public static long macCarthyRecursionAlgorithm (long x) {
        if (x > 100)
            return (x-10);
        else
            return macCarthyRecursionAlgorithm(macCarthyRecursionAlgorithm(x + 11));
    }
}
