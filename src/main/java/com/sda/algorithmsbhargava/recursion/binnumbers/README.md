Napisz program, który wypisze na ekranie reprezentację binarną zadanej liczby całkowitej dodatniej. 
Zadanie zrealizuj z użyciem rekurencji.

Przykład: dla liczby 15 program powinien wypisać 1111.