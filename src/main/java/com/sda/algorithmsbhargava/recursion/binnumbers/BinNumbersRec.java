package com.sda.algorithmsbhargava.recursion.binnumbers;

public class BinNumbersRec {

    public int binNumbersWithRec(int n) {

        if (n == 0)
            return 0;

        else
            return (n % 2 + 10 * binNumbersWithRec(n / 2));
    }

    public void binNumbersWithRecSecondVer(int n) {

        if (n == 0) {
            System.out.print("0");

        } else {
            binNumbersWithRecSecondVer(n / 2);
            System.out.print(n % 2);
        }
    }

}




