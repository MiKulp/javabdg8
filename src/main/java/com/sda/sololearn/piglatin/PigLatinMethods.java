package com.sda.sololearn.piglatin;

public class PigLatinMethods {

    public String PigLatinTranslator(String string) {
        String[] array = makeArrayFromString(string);
        StringBuilder resultSet = new StringBuilder();

        for (String s : array) {
            StringBuilder builder = new StringBuilder(addLettersToString(removeFirstLetterAndAddInTheEndOfString(s)));
            resultSet.append(builder).append(' ');
        }
        return resultSet.toString();
    }

    public String[] makeArrayFromString(String string) {
        String[] array = string.split(" ");
        return array;
    }

    public String removeFirstLetterAndAddInTheEndOfString(String string) {

        char[] array = string.toCharArray();
        char[] returnArray = new char[string.length()];

        returnArray[string.length() - 1] = array[0];
        for (int i = 0 ; i < string.length() - 1 ; i++) {
            returnArray[i] = array[i + 1];
        }

        String returnSet = String.valueOf(returnArray);
        return returnSet;
    }

    public String addLettersToString(String string) {
        char[] array = new char[string.length() + 2];

        for (int i = 0 ; i < string.length() ; i++) {
            array[i] = string.charAt(i);
        }

        array[string.length()] = 'a';
        array[string.length() + 1] = 'y';

        String stringScore = String.valueOf(array);
        return stringScore;

    }


}
