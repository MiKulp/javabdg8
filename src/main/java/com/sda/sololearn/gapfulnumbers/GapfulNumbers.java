package com.sda.sololearn.gapfulnumbers;

public class GapfulNumbers {

    public boolean isGapfulNumbers(String number) {
        char[] numberArray = number.toCharArray();
        int dividend,  divider, score;

        if (numberArray.length < 3) {
            System.out.println("Liczba musi mieć co najmniej trzy cyfry. Żegnam. ");
            System.exit(1);
        }

        StringBuilder builder = new StringBuilder();
        char firstNumber = numberArray[0], lastNumber = numberArray[numberArray.length - 1];

        builder.append(firstNumber);
        builder.append(lastNumber);

        dividend = Integer.valueOf(number);
        divider = Integer.valueOf(builder.toString());

        score = dividend % divider;

        System.out.println((score != 0) ? "Podana liczba nie jest GN" : "Podana liczba to GN");
        return ((score != 0) ? false : true);
    }
}
