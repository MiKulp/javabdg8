package com.sda.optional.ex02;

public class Main {
    public static void main(String[] args) {
        String text = "12345 789";
        String textNull = null;
        TestLengthWithOptional tlwo = new TestLengthWithOptional();

        tlwo.stringLnthWithAnotherMethod(text);
        tlwo.stringLength(text);
    }
}
