package com.sda.optional.ex02;

import java.util.Optional;

public class TestLengthWithOptional {

    public Integer stringLength(String string) {
        Integer score = null;
        Optional<String> functionOptional = Optional.of(string);
        score = functionOptional.isPresent() ? string.length() : 0;
        return score;
    }

    public Integer stringLnthWithAnotherMethod(String string) {
        Integer score = null;
        Optional<String> functionOptional = Optional.ofNullable(string);
        score = functionOptional.map(String::length).orElse(-1);
        return score;
    }
}
