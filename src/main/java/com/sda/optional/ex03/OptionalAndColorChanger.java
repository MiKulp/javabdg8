package com.sda.optional.ex03;

import java.util.Optional;

public class OptionalAndColorChanger {

    public Colors colorConverter(String kolor) {
        Colors koloryWynik = null;
        Optional<String> optional = Optional.empty();
        optional = Optional.ofNullable(kolor);
        koloryWynik = Colors.valueOf((optional.map(String::valueOf).orElse("UNKNOWN")).toUpperCase());

        return koloryWynik;
    }

}
