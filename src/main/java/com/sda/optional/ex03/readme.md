Przygotuj metodę która konwertuje nazwę koloru na enumerator. 
Niech enumerator posiada 4 kolory: red, green, blue oraz unknown. 
Jeśli do metody zostanie przekazany pusty (null) String to przekonwertuj podany kolor na unknown.

__Podpowiedź
Korzystając z metody map oraz valueOf przekonwertuj wartość tekstową na Enum. 
Następnie korzystając z metody orElse zwróć kolor unknown.`__