package com.sda.optional.ex01;

import java.util.Optional;

public class OptionalNullTester {

    public boolean isNull (Object object) {
        boolean wynik;
        Optional <Object> optional = Optional.ofNullable(object);
        wynik = optional.isEmpty();
        return wynik;
    }
}
