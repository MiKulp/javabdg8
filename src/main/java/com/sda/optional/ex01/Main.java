package com.sda.optional.ex01;

public class Main {

    public static void main(String[] args) {
        Object object = null;
        String string = "someText";
        OptionalNullTester ont = new OptionalNullTester();
        System.out.println(ont.isNull(object));
        System.out.println(ont.isNull(string));
    }

}
