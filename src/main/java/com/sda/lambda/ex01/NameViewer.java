package com.sda.lambda.ex01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NameViewer {

    public void listViewer(List<String> list) {
       list.forEach(s -> System.out.println(s));
    }
}
