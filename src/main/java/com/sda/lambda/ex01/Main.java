package com.sda.lambda.ex01;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List <String> nameList = Arrays.asList(
                "Adam",
                "Ewa",
                "Marek",
                "Mateusz",
                "Łukasz",
                "Jan"
        );

        NameViewer nameViewer = new NameViewer();
        nameViewer.listViewer(nameList);

    }
}
