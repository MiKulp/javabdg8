Utwórz klasę sortującą osoby na podstawie środków zgromadzonych na kontach.
Metodę sortującą umieść w osobnej klasie, tak aby można było łatwo wykonać testy
jednostkowe. Wykonaj zadanie zgodnie z poniższymi punktami.

a. Utwórz klasę BankAccount zawierającą pola:

i. name typu String

ii. balance typu Double

b. Utwórz klasę Person zawierającą pola:

i. name typu String

ii. lastName typu String

iii. accounts typu List<BankAccount>

c. Utwórz 5 osób posiadających co najmniej 2 konta na liście accounts.

d. Dodaj utworzone osoby do listy List<paerson>

e. Za pomocą strumieni posortuj osoby według sumy środków zgromadzonych na posiadanych kontach persons.stream().sorted(<wyrażenie lambda>);

f. Wyświetl posortowane osoby.

g. Przygotuj testy jednostkowe sprawdzające metodę sortowania.