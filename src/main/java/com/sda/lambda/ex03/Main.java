package com.sda.lambda.ex03;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<BankAccount> bankAccountsMK = new ArrayList<>();
        bankAccountsMK.add(new BankAccount("Michał", 100.0));
        bankAccountsMK.add(new BankAccount("Michał", 200.0));
        bankAccountsMK.add(new BankAccount("Michał", 900.0));
        bankAccountsMK.add(new BankAccount("Michał", 151.0));
        bankAccountsMK.add(new BankAccount("Michał", 2.0));
        bankAccountsMK.add(new BankAccount("Michał", 150.0));

        List<BankAccount> bankAccountsAW = new ArrayList<>();
        bankAccountsAW.add(new BankAccount("Adam", 152.3));
        bankAccountsAW.add(new BankAccount("Adam", 2000000.0));

        List<BankAccount> bankAccountsKN = new ArrayList<>();
        bankAccountsKN.add(new BankAccount("Kasia", 199.9));
        bankAccountsKN.add(new BankAccount("Kasia", 90000.3));

        List<BankAccount> bankAccountsJS = new ArrayList<>();
        bankAccountsJS.add(new BankAccount("Jacek", 0.9));
        bankAccountsJS.add(new BankAccount("Jacek", 15000.0));

        List<BankAccount> bankAccountsKS = new ArrayList<>();
        bankAccountsKS.add(new BankAccount("Kaja", 199.0));
        bankAccountsKS.add(new BankAccount("Kaja", 139.9));

        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Michał", "Ko", bankAccountsMK));
        persons.add(new Person("Adam", "Mi", bankAccountsAW));
        persons.add(new Person("Kasia", "No", bankAccountsKN));
        persons.add(new Person("Jacek", "St", bankAccountsJS));
        persons.add(new Person("Kaja", "Si", bankAccountsKS));

        ClassWithCompareEngine engine = new ClassWithCompareEngine();
        bankAccountsMK = engine.listAfterSort(bankAccountsMK);

        engine.personListSorting(persons);


    }
}
