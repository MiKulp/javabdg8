package com.sda.lambda.ex03;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Setter
@Getter
@ToString

public class BankAccount {

    String name;
    Double balance; // saldo

    public BankAccount(String name, Double balance) {
        this.name = name;
        this.balance = balance;
    }
}
