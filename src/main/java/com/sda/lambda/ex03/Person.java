package com.sda.lambda.ex03;

import lombok.*;

import java.util.List;
@Getter
@Setter
@ToString

public class Person {
    String name;
    String lastName;
    List <BankAccount> accounts;

    public Person(String name, String lastName, List<BankAccount> accounts) {
        this.name = name;
        this.lastName = lastName;
        this.accounts = accounts;
    }

    public Person() {
    }
}

