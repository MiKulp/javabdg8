package com.sda.lambda.ex03;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ClassWithCompareEngine {




    public List<BankAccount> listAfterSort(List<BankAccount> unsortedList) {
        List<BankAccount> listAfterSort = new ArrayList<>();
        unsortedList.sort(Comparator.comparing(BankAccount::getBalance));

        listAfterSort = unsortedList.stream().sorted(Comparator.comparing(BankAccount::getBalance)).collect(Collectors.toList());

        return listAfterSort;
    }

    public Double balanceSum (List<BankAccount> listToSum) {

        Double score = 0.0;
        for (int i = 0 ; i < listToSum.size() ; i++) {
            score += listToSum.get(i).getBalance();
        }
        return score;
    }

    public void personListSorting(List<Person> listOfPersons) {

        listOfPersons.forEach(System.out::println);
        System.out.println("XXXXXXXXXXXXXXXXXXX");
        // listOfPersons.stream().forEach(person -> person.getAccounts().stream().sorted((o1, o2) -> o1.getBalance().compareTo(o1.getBalance())).forEach(System.out::println));
        // List <Person> outputList =  listOfPersons.stream().sorted(person -> person.getAccounts().stream().sorted((o1, o2) -> o1.getBalance().compareTo(o1.getBalance())).collect(Collectors.toList()));


        //  listOfPersons.stream().sorted(person -> person.getAccounts().stream().sorted((o1, o2) -> o1.getBalance().compareTo(o2.getBalance())).forEach(System.out::println));


    }
}
