package com.sda.lambda.ex03;

import java.util.Comparator;

public class CustomComparator implements Comparator <BankAccount> {

    @Override
    public int compare(BankAccount o1, BankAccount o2) {
        return o1.balance.compareTo(o2.balance);
    }
}
