package com.sda.lambda.ex02;

import java.util.Comparator;
import java.util.List;

public class SortForPointObjects {


    public void listSorter(List<Point> listForSort) {
        listForSort.sort(Comparator.comparing(point -> point.x));
        listForSort.forEach(point -> System.out.println(point));
    }
}
