package com.sda.lambda.ex02;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List <Point> pointList = Arrays.asList(
                new Point(5,10),
                new Point(2,9),
                new Point(7,8),
                new Point(4,7),
                new Point(11,6),
                new Point(6,5),
                new Point(3,4),
                new Point(8,3),
                new Point(9,2),
                new Point(10,1)
        );

        SortForPointObjects sortForPointObjects = new SortForPointObjects();
        sortForPointObjects.listSorter(pointList);
    }
}
