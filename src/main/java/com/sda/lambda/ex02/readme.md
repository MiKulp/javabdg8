Utwórz klasę _Point_ zawierającą zmienne int _x_ oraz int _y_. 
Dodaj 10 punktów do listy _List<Point>_. 
Korzystając ze strumieni posortuj punkty według współrzędnej _x_ i
wyświetl posortowaną tablicę.