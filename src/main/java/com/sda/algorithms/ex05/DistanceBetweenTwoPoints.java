package com.sda.algorithms.ex05;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class DistanceBetweenTwoPoints {
    PointInCoordinateSystem pointFirst;
    PointInCoordinateSystem pointSecond;
    Double distance;
}
