Przygotuj aplikację która znajduje parę punktów położonych najbliżej siebie w
układzie współrzędnych x,y.

Dane wejściowe: Punkty wprowadzane przez konsole lub losowane.

Dane wyjściowe: 2 punkty położone najbliżej siebie.

Materiały pomocnicze: 
http://matematyka.pisz.pl/strona/1248.html

a. Przygotuj wersję poszukującą najbliżej położone punkty metodą zachłanną
czyli porównującą każdy punkt z każdym szukając najbliżej położonych.

b. *****Przygotuj wersję wyszukującą najbliżej położone punkty metodą “dziel i
zwyciężaj”