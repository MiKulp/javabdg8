package com.sda.algorithms.ex05;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        DistanceInCoordinateSystem distance = new DistanceInCoordinateSystem();

        List<PointInCoordinateSystem> listWithScoreFinal = distance.randomPointsInCoordinateSystem();

        long start = System.currentTimeMillis();
        List<DistanceBetweenTwoPoints> listInMainWithScore = distance.distanceInRandomListFactory(listWithScoreFinal);
        long stop = System.currentTimeMillis();
        float wynik = stop-start;
        wynik=wynik/1000;

        System.out.println(wynik);

        for (DistanceBetweenTwoPoints distanceSc : listInMainWithScore
        ) {
            System.out.println(distanceSc);
        }
    }
}
