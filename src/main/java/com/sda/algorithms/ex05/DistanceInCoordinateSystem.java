package com.sda.algorithms.ex05;

import javax.print.DocFlavor;
import java.util.*;

public class DistanceInCoordinateSystem {

    public double distanceBetweenPointsInCoordinateSystem(PointInCoordinateSystem firstPoint, PointInCoordinateSystem secondPoint) {
        double a = Math.pow((secondPoint.x - firstPoint.x), 2);
        double b = Math.pow((secondPoint.y - firstPoint.y), 2);
        return Math.sqrt(a + b);
    }

    public Map<PointInCoordinateSystem, Double> pointMaker() {
        Map<PointInCoordinateSystem, Double> mapWithPoints = new HashMap<>();

        for (int i = 1 ; i <= 100 ; i++) {

            for (int j = 1 ; j <= 100 ; j++) {
                mapWithPoints.put(new PointInCoordinateSystem(i, j), 0.0);
            }
        }
        return mapWithPoints;
    }

    public Map<PointInCoordinateSystem, Double> pointWithLengthValuesFactory(Map<PointInCoordinateSystem, Double> map, PointInCoordinateSystem point) {

        double values;
        for (PointInCoordinateSystem pointInCoordinateSystem : map.keySet()
        ) {
            values = distanceBetweenPointsInCoordinateSystem(pointInCoordinateSystem, point);
            map.replace(pointInCoordinateSystem, values);
        }
        return map;
    }

    public List<PointInCoordinateSystem> randomPointsInCoordinateSystem() {
        List<PointInCoordinateSystem> listWithPoints = new ArrayList<>();
        Random random = new Random();

        for (int i = 1 ; i <= 10000 ; i++) {
            listWithPoints.add(new PointInCoordinateSystem(random.nextInt(100000), random.nextInt(100000)));
        }
        return listWithPoints;
    }

    //public Map<DistanceBetweenTwoPoints,Double>

    public List<DistanceBetweenTwoPoints> distanceInRandomListFactory(List<PointInCoordinateSystem> list) {
        List<DistanceBetweenTwoPoints> listWithLastScore = new ArrayList<>();
        double score;
        PointInCoordinateSystem point;
        DistanceBetweenTwoPoints dbtw;

        for (int i = 0 ; i < list.size() ; i++) {
            for (int j = 0 ; j < list.size() && j != i ; j++) {
                score = distanceBetweenPointsInCoordinateSystem(list.get(i), list.get(j));
                dbtw = new DistanceBetweenTwoPoints(list.get(i), list.get(j), score);

                if (listWithLastScore.size() == 0) {
                    listWithLastScore.add(dbtw);
                }

               else if (listWithLastScore.get(0).distance > dbtw.distance) {
                    listWithLastScore.remove(0);
                }
            }
        }
        return listWithLastScore;
    }
}
