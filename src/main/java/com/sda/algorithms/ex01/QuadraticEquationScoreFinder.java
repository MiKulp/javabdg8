package com.sda.algorithms.ex01;

public class QuadraticEquationScoreFinder {

    public double deltaFinder(double a, double b, double c) {
        return (Math.pow(b, 2) - (4 * a * c));
    }

    public double firstArgumentCounter(double a, double b, double delta) {

        return ((-b) - Math.sqrt(delta)) / 2 * a;
    }

    public double secondArgumentCounter(double a, double b, double delta) {

        return ((-b) + Math.sqrt(delta)) / 2 * a;
    }

    public double onlyOneArgumentCounter(double a, double b) {

        return  (-b)/(2*a);
    }

    public void quadraticEquation(double a, double b, double c) {
        double delta = deltaFinder(a, b, c);

        if (delta > 0) {
            System.out.printf("Równanie ma dwa rozwiązania. %n");
            System.out.printf(" x1 = %3.2f %n ", firstArgumentCounter(a, b,delta));
            System.out.printf("x2 = %3.2f %n ", secondArgumentCounter(a,b,delta));

        } else if (delta < 0) {
            System.out.println("Równanie nie posiada rozwiązania. ");

        } else if (delta == 0) {
            System.out.println("Równanie ma tylko jedno rozwiązanie. ");
            System.out.printf(" x1 = %3.2f %n ", onlyOneArgumentCounter(a,b));

        }
    }
}
