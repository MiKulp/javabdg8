package com.sda.algorithms.ex01;

import java.util.Scanner;

 class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj parametry równania kwadratowegp postaci ax2+bx+c=0");
        System.out.println("Wpisz wartość argumentu a. ");
        double a = sc.nextDouble();
        System.out.println("Wpisz wartość argumentu b. ");
        double b = sc.nextDouble();
        System.out.println("Wpisz wartość argumentu c. ");
        double c = sc.nextDouble();

        QuadraticEquationScoreFinder finder = new QuadraticEquationScoreFinder();
        finder.quadraticEquation(a, b, c);


    }


}
