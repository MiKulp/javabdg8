package com.sda.recursion.ex04;

public class FibonacciString {

    public int fibonacciStringWithRec(int n) {

        if (n == 0)
            return 0;
        else if (n == 1)
            return 1;
        else {
            return fibonacciStringWithRec(n - 2) + fibonacciStringWithRec(n - 1);
        }
    }

    public int fibonacciStrinWithLoop(int n) {
        int a = 0;
        int b = 1;
        int wynik = 1;

        if (n <= 1)
            wynik = n;
        else  {
            for (int i = 1 ; i < n ; i++) {
                wynik = a + b;
                a=b;
                b=wynik;
            }
        }
         return wynik;
    }
}
