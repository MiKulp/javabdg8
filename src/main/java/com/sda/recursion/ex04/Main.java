package com.sda.recursion.ex04;

public class Main {
    public static void main(String[] args) {

        FibonacciString fibonacciString = new FibonacciString();
        int testNumber = 30;

        long startRec = System.currentTimeMillis();
        long scoreRec = fibonacciString.fibonacciStringWithRec(testNumber);
        long stopRec = System.currentTimeMillis();

        long startLoop = System.currentTimeMillis();
        long scoreLopp = fibonacciString.fibonacciStrinWithLoop(testNumber);
        long stopLoop = System.currentTimeMillis();

        System.out.printf("Czas obliecznie %d elementu ciągu Fibonacciego, który wynosi %d ,  za pomocą rekursji wynosi %d %n", testNumber, scoreRec, stopRec - startRec);
        System.out.printf("Czas obliecznie %d elementu ciągi Fibonacciego, który wynosi %d ,   za pomocą pętli wynosi %d", testNumber, scoreLopp, stopLoop - startLoop);
    }
}
