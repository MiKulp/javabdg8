package com.sda.recursion.ex03;

public class PowerEngine {

    public long powerWithRecursion(long n) {

        if (n <= 1) {
            return 1;
        } else
            return n * powerWithRecursion(n - 1);
    }

    public long powerWithoutRecursion (long n) {

        long wynik = 1;
        if (n <= 1) {
            return 1;

        }else {
            for (long i = 1 ; i <= n ; i++) {
                wynik *= i;
            }
            return wynik;
        }

    }
}
