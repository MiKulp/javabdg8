package com.sda.recursion.ex03;

public class Main {
    public static void main(String[] args) {
        int x = 60;
        long x2 = 10L;

        PowerEngine pe = new PowerEngine();

        long start = System.currentTimeMillis();
        long recursionPower = pe.powerWithRecursion(x2);
        long stop = System.currentTimeMillis();

        long startWithoutRec = System.currentTimeMillis();
        long loopPower = pe.powerWithoutRecursion(x2);
        long stopWithoutRec = System.currentTimeMillis();

        long scoreRec = stop - start;
        long scoreLoop = startWithoutRec - startWithoutRec;


        System.out.printf("Czas obliczenia silni w algorytmie z rekurencją dla %d wyniósł %d ms. %n Wynik to %d %n ", x2, scoreRec, recursionPower);
        System.out.printf("Czas obliczenia silni w algorytmie bez rekurencji dla %d wyniósł %d ms. %n Wynik to %d %n", x2, scoreRec, loopPower);
    }
}
