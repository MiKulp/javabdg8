package com.sda.recursion.ex01;

import java.util.Random;

public class RecursionEngine {

    StringBuilder st = new StringBuilder("");

    public String randomStringMaker(int n) {
        Random random = new Random();
        StringBuilder str = new StringBuilder("");

        for (int i = 0 ; i < n ; i++) {
            str.append(random.nextInt(9));
        }
        return str.toString();
    }

    public String reverseOrderWithoutRecursion(String string) {
        for (int i = string.length() - 1 ; i >= 0 ; i--) {
            st.append(string.charAt(i));
        }
        return st.toString();
    }

    public String reverseOrderWithRecursion(String s) {
        if (s.length() > 0) {
            st.append(s.charAt(s.length() - 1));
            reverseOrderWithRecursion(s.substring(0, s.length() - 1));
        }
        return st.toString();
    }
}
