package com.sda.recursion.ex01;

public class Main {
    public static void main(String[] args) {

        RecursionEngine re = new RecursionEngine();
        String s = re.randomStringMaker(4300);

        long start = System.currentTimeMillis();
        re.reverseOrderWithoutRecursion(s);
        long stop = System.currentTimeMillis();

        long startRecursion = System.currentTimeMillis();
        re.reverseOrderWithRecursion(s);
        long stopRecursion = System.currentTimeMillis();

        System.out.println("Czas wydruku bez rekursji " + (stop - start));
        System.out.println("Czas wydruku z rekursją " + (stopRecursion - startRecursion));
    }
}
