package com.sda.recursion.ex02;

public class CounterForNumbers {

    public int countNumbersWithRecursion(int n) {

        while (n == 0) {
            return n;
        }
        return n + (countNumbersWithRecursion(n - 1));
    }

    public int countNumbersWithoutRecursion(int n) {
        int wynik = 0;

        for (int i = 0 ; i <= n ; i++) {
            wynik += i;
        }
        return wynik;
    }
}
