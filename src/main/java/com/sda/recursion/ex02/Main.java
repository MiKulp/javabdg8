package com.sda.recursion.ex02;

public class Main {


    public static void main(String[] args) {
        CounterForNumbers counterForNumbers = new CounterForNumbers();
        int n = 10000;

        long start = System.currentTimeMillis();
        counterForNumbers.countNumbersWithRecursion(n);
        long stop = System.currentTimeMillis();
        System.out.println("Czas wykonania zadanie przy użyciu rekurencji to " + (stop - start));


        long startWithoutRec = System.currentTimeMillis();
        counterForNumbers.countNumbersWithoutRecursion(n);
        long stopWithoutRec = System.currentTimeMillis();
        System.out.println("Czas wykonania zadanie w pętli to " + (stopWithoutRec - startWithoutRec));


    }
}
