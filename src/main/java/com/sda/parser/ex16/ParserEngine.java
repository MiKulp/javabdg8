package com.sda.parser.ex16;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserEngine {

    public String takeHTMLtoString(String link) throws IOException {

        String wynik;
        URLConnection connection;
        connection = new URL(link).openConnection();
        Scanner scanner = new Scanner(connection.getInputStream());
        scanner.useDelimiter("\\Z");
        wynik = scanner.next();
        scanner.close();

        return wynik;
    }

    public void picturesFroHTMLprinter(String string) throws IOException {

        String REGEX = "img src=\".+\"";
        Pattern pattern = Pattern.compile(REGEX);
        String stringToMatch = takeHTMLtoString(string);
        Matcher matcher = pattern.matcher(stringToMatch);

        while (matcher.find()) {
            System.out.println(matcher.group());
        }

    }

}
