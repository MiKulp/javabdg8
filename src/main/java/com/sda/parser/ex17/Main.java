package com.sda.parser.ex17;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String HTMLaddress = "https://www.wp.pl/";
        ParserEngineWWW parserEngineWWW = new ParserEngineWWW();
        parserEngineWWW.takeWWWaddressesFroHTMLprinter(HTMLaddress);
    }
}
