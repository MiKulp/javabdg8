package com.sda.parser.ex17;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserEngineWWW {

    public String takeHTMLtoString(String link) throws IOException {

        String wynik;
        URLConnection connection;
        connection = new URL(link).openConnection();
        Scanner scanner = new Scanner(connection.getInputStream());
        scanner.useDelimiter("\\Z");
        wynik = scanner.next();
        scanner.close();

        return wynik;
    }

    public void takeWWWaddressesFroHTMLprinter(String string) throws IOException {

        String REGEX = "((www\\.|(www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|(www\\.|(www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})";
        Pattern pattern = Pattern.compile(REGEX);
        String stringToMatch = takeHTMLtoString(string);
        Matcher matcher = pattern.matcher(stringToMatch);


        while (matcher.find()) {
            System.out.println(matcher.group());
        }

    }
}
