package com.sda.parser.ex18;

import lombok.SneakyThrows;

import java.util.List;

public class Main {

    @SneakyThrows
    public static void main(String[] args) {

        String filename = "C:\\Users\\mkulp\\Desktop\\MOCK_DATA.csv";
        ParserPersonEngine parserPersonEngine = new ParserPersonEngine();

       List<Person> listOfPersons = parserPersonEngine.readPersonsFromCSV(filename);

        for (Person p: listOfPersons
             ) {
            System.out.println(p);
        }
    }
}
