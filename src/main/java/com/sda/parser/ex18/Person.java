package com.sda.parser.ex18;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString

public class Person {
    String name;
    String surname;
    String email;
    int age;
}
