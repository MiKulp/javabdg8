package com.sda.parser.ex18;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ParserPersonEngine {

    public List<Person> readPersonsFromCSV(String fileName) throws IOException {
        List<Person> persons = new ArrayList<>();
        Path pathToCSVfile = Paths.get(fileName);

        try (BufferedReader br = Files.newBufferedReader(pathToCSVfile, StandardCharsets.UTF_8)) {
            String line = br.readLine();

            while (line != null) {

                String[] personAttributes = line.split(",");
                Person person = createPerson(personAttributes);
                persons.add(person);
                line = br.readLine(); //????
            }
        }
        return persons;
    }

    public Person createPerson(String[] data) {

        String name = data[0];
        String surname = data[1];
        String email = data[2];
        int age = Integer.parseInt(data[3]);
        return new Person(name, surname, email, age);
    }
}
