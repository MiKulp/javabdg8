package com.sda.regex.ex06;

import java.util.regex.Pattern;

public class ValidatorString {

    public boolean validator(String string) {

        String REGEX = "[a-zA-Z0-9]*";
        Pattern pattern = Pattern.compile(REGEX);
        boolean wynik = pattern.matcher(string).matches();
        return wynik;
    }
}
