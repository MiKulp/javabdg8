package com.sda.regex.ex04;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckDigitNumber {

    public boolean haveOnlyThreeDigits(String string) {
        String REGEX = "[\\d]{3}";
        Pattern pattern = Pattern.compile(REGEX);
        boolean wynik = pattern.matcher(string).matches();
        return wynik;
    }
}
