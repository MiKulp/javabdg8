package com.sda.regex.ex01;

public class Main {
    public static void main(String[] args) {

        String example = "ABCD";
        ValidatorLowerUpperCase validatorLowerUpperCase = new ValidatorLowerUpperCase();

        System.out.println(
                validatorLowerUpperCase.isOnlyLower(example));

    }
}
