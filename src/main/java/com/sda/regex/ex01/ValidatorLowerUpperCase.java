package com.sda.regex.ex01;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorLowerUpperCase {

    public boolean isOnlyLower(String text) {

        String validator = "[A-Z]+";
        Pattern pattern = Pattern.compile(validator);
        boolean wynik = pattern.matcher(text).matches();

        return wynik;
    }


}


