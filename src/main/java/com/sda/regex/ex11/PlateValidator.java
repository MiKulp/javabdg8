package com.sda.regex.ex11;

import java.util.regex.Pattern;

public class PlateValidator {

    public boolean validate(String plate) {

        String REGEX = "[A-Z]{2}((\\d{5})|(\\d{4}[A-Z]{1})|(\\d{3}[A-Z]{2}))";
        Pattern pattern = Pattern.compile(REGEX);
        boolean wynik = pattern.matcher(plate).matches();
        return wynik;

    }

}
