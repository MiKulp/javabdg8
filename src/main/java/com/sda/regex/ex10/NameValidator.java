package com.sda.regex.ex10;

import java.util.regex.Pattern;

public class NameValidator {

    public boolean validate (String name) {
        String REGEX = "[[A-ZĄĆĘŁŃÓŚŻŻ]{1}][a-ząćęłńóśźż]+";
        Pattern pattern = Pattern.compile(REGEX);
        boolean wynik = pattern.matcher(name).matches();

        return wynik;
    }
}
