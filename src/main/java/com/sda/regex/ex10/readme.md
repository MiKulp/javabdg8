Przygotuj aplikację sprawdzającą czy wprowadzone imię jest poprawne. 
Użytkownik wprowadza tekst w postaci “Jan”, a następnie program sprawdza jego poprawność. 
Przygotuj klasę o nazwie NameValidator posiadającą metodę

`public boolean validate(String name);`

Poprawne imię to: Jan, Monika, Łukasz, Krzysztof 
Błędne imię to: jan, monika, jan87, Jan87 Imię uważa się za poprawne, 
gdy zawiera tylko litery i rozpoczyna się wielką literą.

Podpowiedź
Sprawdzenie wykonaj za pomocą Optional....of....filter
Do sprawdzenia czy hasło zawiera określone znaki wykorzystaj metodę
matchesklasyString